﻿using System;
using System.Collections.Generic;

namespace Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        List<TEntity> All { get;  }
        TEntity Get(int id);
        void Add(TEntity entity);
        void Remove(TEntity entity);
        void Remove(int id);
        void Update(TEntity entity);
    }
}
