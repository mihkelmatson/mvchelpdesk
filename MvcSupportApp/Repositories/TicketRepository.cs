﻿using Interfaces;
using MvcSupportApp.Data;
using MvcSupportApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcSupportApp.Repositories
{
    public class TicketRepository : IRepository<Ticket>
    {

        public TicketContext context { get; set; }
        public List<Ticket> All => context.Tickets.ToList();

        public void Add(Ticket entity)
        {
            context.Tickets.Add(entity);
            context.SaveChanges();
        }

        public Ticket Get(int id)
        {
            return context.Tickets.Find(id);
        }

        public void Remove(Ticket entity)
        {
            var obj = context.Tickets.Find(entity.Id);
            context.Tickets.Remove(obj);
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            var obj = context.Tickets.Find(id);
            context.Tickets.Remove(obj);
            context.SaveChanges();
        }

        public void Update(Ticket ticket)
        {
            context.Tickets.Update(ticket);
            context.SaveChanges();
        }

    }
}
