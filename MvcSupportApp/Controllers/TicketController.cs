﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MvcSupportApp.Models;
using MvcSupportApp.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Interfaces;

namespace MvcSupportApp.Controllers
{
    public class TicketController : Controller
    {

        //private readonly TicketContext _context;

        ////using dependency injection
        //public TicketController(TicketContext context)
        //{
        //    _context = context;
        //}

        //Property of the type IRepository <TEnt>
        private IRepository<Ticket> _repository;

        //The Dependency Injection of the IRepository<TEnt>
        public TicketController(IRepository<Ticket> repo)
        {
            _repository = repo;
        }

        /// <summary>
        /// GET: Tickets, Gets all tickets from the list
        /// </summary>
        /// <returns>List of tickets</returns>
        public ActionResult Index()
        {
            var ticks =  _repository.All.Where(x => x.IsOpen == true).OrderBy(x => x.Due);
            return View(ticks);
            //return View(await _repository.Tickets.Where(x => x.IsOpen == true).OrderBy(x => x.Due).ToListAsync());
        }


        /// <summary>
        /// GET: Create ticket view
        /// </summary>
        /// <returns>A form to create ticket</returns>
        public ActionResult Create()
        {
            //Get the ticket from ticketList sample collection for demo purpose.
            //You can get the ticket from the database in the real application 
            //var tick = ticketList.Where(s => s.Id == TicketId).FirstOrDefault();
            Ticket tick = new Ticket();
            return View(tick);
        }

        /// <summary>
        /// POST: Ticket, Creates new ticket
        /// </summary>
        /// <returns>Index view, if all correct</returns>
        [HttpPost]
        public ActionResult Create(Ticket tick)
        {
            var  tickTmp = new Ticket
            {
                Title = tick.Title,
                Created = tick.Created,
                Due = tick.Due,
                IsOpen = true
            };

            if (ModelState.IsValid)
            {
                //write code to update ticket 
                //_context.Tickets.Add(tickTmp);
                //_context.SaveChanges();
                //return RedirectToAction("Index");
                _repository.Add(tickTmp);
                return RedirectToAction("Index");
            }
            return View(tickTmp);
        }

        /// <summary>
        /// GET: Ticket, Deletes a ticket with id
        /// </summary>
        /// <returns>Index view, if all correct</returns>
        [HttpGet]
        [Route("Ticket/Close/{tickID:int}")]
        public IActionResult Close(int tickId)
        {
            //var tickTmp =  _context.Tickets.First(m => m.Id == tickId);
            var tickTmp2 = _repository.Get(tickId);
            if (tickTmp2 == null)
            {
                return NotFound();
            }
            tickTmp2.IsOpen = false;

            _repository.Update(tickTmp2);
            return RedirectToAction("Index");
            //_context.Tickets.Update(tickTmp);
            //_context.SaveChanges();
            //return RedirectToAction("Index");
        }
    }
}
