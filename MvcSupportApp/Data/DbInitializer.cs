﻿using MvcSupportApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcSupportApp.Data
{
    public static class DbInitializer
    {
        public static void Initialize(TicketContext context)
        {
            context.Database.EnsureCreated();

            // Look for any tickets.
            if (context.Tickets.Any())
            {
                return;   // DB has been seeded
            }

            var tickets = new Ticket[]
            {
                new Ticket{ Title="Ticket 1", Created=DateTime.Parse("2005-09-01"), Due=DateTime.Parse("2005-09-10"), IsOpen=true },
                new Ticket{ Title="Ticket 2", Created=DateTime.Parse("2017-07-21"), Due=DateTime.Parse("2017-07-21"), IsOpen=false },
                new Ticket{ Title="Ticket 3", Created=DateTime.Parse("2017-07-21"), Due=DateTime.Parse("2017-08-29"), IsOpen=true },
                new Ticket{ Title="Ticket 4", Created=DateTime.Parse("2017-07-21"), Due=DateTime.Parse("2017-07-21"), IsOpen=true }
            };

            foreach (Ticket tick in tickets)
            {
                context.Tickets.Add(tick);
            }


            context.SaveChanges();
        }
    }
}
